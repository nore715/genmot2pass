#!/usr/bin/env python
#-*- coding: utf-8 -*-

import os,sys,argparse,string
from random import *

HardCodedLimit = 8
NbOfPwd = 1


############### Définition de la fonction de génération de mot de passe
def genpassword(min,max,uc,lc,nb,sc):
    maju = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    minu = "abcdefghijklmnopqrstuvwxyz"
    chif = "0123456789"
    spec = "+-*$%&.:?!"
        ## print "Reçu", min, max, uc, lc, nb, sc ##Debug
        # Lets create pop as requested
        pop = ''
        if(uc == 0):
            pop += maju
        if(lc == 0):
            pop += minu
        if(nb == 0):
            pop += chif
        if(sc == 0):
            pop += spec
        if(uc==0 and lc==0 and nb==0 and sc==0):
            ## print "Tout est à zéro" ##Debug
            pop = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-*$%&.:?!"
        ## print "pop retenu",pop ##Debug
    # Boucle pour être sûr de générer un mot de passe correct
    while 1:
        # Initialize vars
        c = 0
        passwd = ''
        nbmaju = 0
        nbminu = 0
        nbchif = 0
        nbspec = 0
        # random length
        longueur = randrange(min,max)
        print longueur
        # Gen password loop
        while c < longueur:
            caractere = pop[randrange(len(pop))]
            if caractere in maju:
                nbmaju += 1
            if caractere in minu:
                nbminu += 1
            if caractere in chif:
                nbchif += 1
            if caractere in spec:
                nbspec += 1
            passwd += caractere
            c += 1
        break
        # Condition de validité du mot de passe : il doit contenir au moins 3 des 4 classes de caractères parmis nb*
        # (A|B) & (A|C) & (A|D) & (B|C) & (B|D) & (C|D)
        #if (bool(nbmaju) or bool(nbminu)) and (bool(nbmaju) or bool(nbchif)) and (bool(nbmaju) or bool(nbspec)) and (bool(nbminu) or bool(nbchif)) and (bool(nbminu) or bool(nbspec)) and (bool(nbchif) or bool(nbspec)):
        return passwd
###################

parser = argparse.ArgumentParser()
parser.add_argument("-n", "--number",
                    type=int,
                    default=1,
                    help="Number of random strings to generate")
parser.add_argument("-m", "--min",
                    type=int,
                    default=HardCodedLimit,
                    help="Minimum number of characters in random string")
parser.add_argument("-M", "--max",
                    type=int,
                    default=HardCodedLimit,
                    help="Maximum number of characters in random string")
parser.add_argument("-U", "--upper",
                    type=int,
                    default=0,
                    help="No upper case character in random string")
parser.add_argument("-L", "--lower",
                    type=int,
                    default=0,
                    help="No lower case character in random string")
parser.add_argument("-D", "--digit",
                    type=int,
                    default=0,
                    help="No digit in random string")
parser.add_argument("-S", "--specialchar",
                    type=int,
                    default=0,
                    help="No special character in random string")
args = parser.parse_args()

if args.number < 1:
    print "The number of required random strings cannot be null or negative"
elif args.min < HardCodedLimit:
    print "The minimum number of characters cannot be lower than ",HardCodedLimit
elif args.max < args.min:
    print "The max nb of char in random string cannot be lower than the min nb of chars"
else:
    nb = 0
    print "You've asked for",args.number,"passwords between",args.min,"and",args.max,"characters."
    print ""
    while nb < args.number:
        print genpassword(args.min,args.max,args.upper,args.lower,args.digit,args.specialchar)
        nb += 1
